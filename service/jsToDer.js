const asn1 = require("./asn1");
const crypto = require("crypto");
const asn1Tree = require("asn1-tree");
const asn1Mapper = require("asn1-mapper");
const x509 = require("@fidm/x509");
const consts = require("../const/certificates");
const signatureServices = require("../service/signature");
const asn1Services = require("../service/asn1");
const asn_js = require("../Files/RSPDefinitions.min.json");

const eUiccCert = x509.Certificate.fromPEM(consts.euicc.cert.pem.brp);
const eumCert = x509.Certificate.fromPEM(consts.eum.cert.pem.brp);

const transactionId = crypto.randomBytes(16);

const smdpSigned2 = {
  transactionId,
  ccRequiredFlag: Buffer.from([false]),
  bppEuiccOtpk: eumCert.publicKeyRaw,
};

const smdpSigned2Buffer = asn1.jsToDer(smdpSigned2, asn_js.SmdpSigned2);

console.log(smdpSigned2Buffer);
