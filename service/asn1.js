"use strict";

const asn1Tree = require("asn1-tree");
const asn1Mapper = require("asn1-mapper");

/**
 * Convert encoded DER string to Js object
 * @param {string} der - Encoded DER string
 * @returns {object} Js object
 */
exports.derToJs = (der, definition) => {
  const buffer = asn1Tree.decode(der);

  const mapped = asn1Mapper.fromTree(buffer, definition);

  return mapped;
};

/**
 * Convert Js object to DER encoded string
 * @param {object} obj - Js object to be converted to DER string
 * @returns {string} DER encoded string of the giving object
 */
exports.jsToDer = (obj, definition) => {
  const tree = asn1Mapper.toTree(obj, definition);

  const buffer = asn1Tree.encode(tree);

  return buffer;
};
