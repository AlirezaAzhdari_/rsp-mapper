const crypto = require("crypto");

/**
 * Generate a digital signature based on the giving data and the algorithm
 */
exports.signData = (data, privateKey) => {
  const signature = crypto.createSign("SHA256");
  signature.update(data);
  signature.end();
  return signature.sign(privateKey, "hex");
};

/**
 * Verify signature over data
 */
exports.verifySignature = (signature, data, publicKey) => {
  const verifier = crypto.createVerify("SHA256");
  verifier.update(data);

  return verifier.verify(publicKey, signature, "hex");
};
