const fs = require("fs");

module.exports = {
  ci: {
    cert: {
      pem: {
        brp: fs.readFileSync(
          "./gsma certificates/Valid test cases/CertificateIssuer/cert.pem"
        ),
        nist: fs.readFileSync(
          "./gsma certificates/Valid test cases/CertificateIssuer/CERT_CI_ECDSA_NIST.pem"
        ),
      },
      der: {
        brp: fs.readFileSync(
          "./gsma certificates/Valid test cases/CertificateIssuer/CERT_CI_ECDSA_BRP.der"
        ),
        nist: fs.readFileSync(
          "./gsma certificates/Valid test cases/CertificateIssuer/CERT_CI_ECDSA_NIST.der"
        ),
      },
    },
    key: {
      pem: {
        brp: {
          private: fs.readFileSync(
            "./gsma certificates/Valid test cases/CertificateIssuer/SK_CI_ECDSA_BRP.pem"
          ),
        },
        nist: {
          private: fs.readFileSync(
            "./gsma certificates/Valid test cases/CertificateIssuer/SK_CI_ECDSA_NIST.pem"
          ),
        },
      },
    },
  },
  euicc: {
    cert: {
      pem: {
        brp: fs.readFileSync(
          "./gsma certificates/Valid test cases/eUICC/cert.pem"
        ),
        nist: fs.readFileSync(
          "./gsma certificates/Valid test cases/eUICC/nistCert.pem"
        ),
      },
      der: {
        brp: fs.readFileSync(
          "./gsma certificates/Valid test cases/eUICC/CERT_EUICC_ECDSA_BRP.der"
        ),
        nist: fs.readFileSync(
          "./gsma certificates/Valid test cases/eUICC/CERT_EUICC_ECDSA_NIST.der"
        ),
      },
    },
    key: {
      pem: {
        brp: {
          public: fs.readFileSync(
            "./gsma certificates/Valid test cases/eUICC/PK_EUICC_ECDSA_BRP.pem"
          ),
          private: fs.readFileSync(
            "./gsma certificates/Valid test cases/eUICC/SK_EUICC_ECDSA_BRP.pem"
          ),
        },
        nist: {
          public: fs.readFileSync(
            "./gsma certificates/Valid test cases/eUICC/PK_EUICC_ECDSA_NIST.pem"
          ),
          private: fs.readFileSync(
            "./gsma certificates/Valid test cases/eUICC/SK_EUICC_ECDSA_NIST.pem"
          ),
        },
      },
    },
  },
  eum: {
    cert: {
      pem: {
        brp: fs.readFileSync(
          "./gsma certificates/Valid test cases/EUM/cert.pem"
        ),
        nist: fs.readFileSync(
          "./gsma certificates/Valid test cases/EUM/nistCert.pem"
        ),
      },
      der: {
        brp: fs.readFileSync(
          "./gsma certificates/Valid test cases/EUM/CERT_EUM_ECDSA_BRP.der"
        ),
        nist: fs.readFileSync(
          "./gsma certificates/Valid test cases/EUM/CERT_EUM_ECDSA_NIST.der"
        ),
      },
    },
    key: {
      pem: {
        brp: {
          public: fs.readFileSync(
            "./gsma certificates/Valid test cases/EUM/PK_EUM_ECDSA_BRP.pem"
          ),
          private: fs.readFileSync(
            "./gsma certificates/Valid test cases/EUM/SK_EUM_ECDSA_BRP.pem"
          ),
        },
        nist: {
          public: fs.readFileSync(
            "./gsma certificates/Valid test cases/EUM/PK_EUM_ECDSA_NIST.pem"
          ),
          private: fs.readFileSync(
            "./gsma certificates/Valid test cases/EUM/SK_EUM_ECDSA_NIST.pem"
          ),
        },
      },
    },
  },
  smdp: {
    pem: "./gsma certificates/Valid test cases/SM-DP+/DPpb/cert.pem",
    der: {
      brp: fs.readFileSync(
        "./gsma certificates/Valid test cases/SM-DP+/DPpb/CERT_S_SM_DP2pb_ECDSA_BRP.der"
      ),
      nist: fs.readFileSync(
        "./gsma certificates/Valid test cases/SM-DP+/DPpb/CERT_S_SM_DP2pb_ECDSA_NIST.der"
      ),
    },
  },
};
