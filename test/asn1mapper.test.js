const crypto = require("crypto");
const asn1Tree = require("asn1-tree");
const asn1Mapper = require("asn1-mapper");
const x509 = require("@fidm/x509");
const consts = require("../const/certificates");
const signatureServices = require("../service/signature");
const asn1Services = require("../service/asn1");

const eumCert = x509.Certificate.fromPEM(consts.eum.cert.pem.brp);

const euiccPrivateKey = crypto.createPrivateKey({
  key: consts.euicc.key.pem.nist.private,
  format: "pem",
  type: "pkcs8",
});

const asn_js = require("../dist/RSPDefinitions.min.json");

const tac = Buffer.from("00000000", "hex");
const smdpAddress = Buffer.from("testdefaultsmdpplus1.gsma.com", "utf-8");
const euiccChallenge = Buffer.from("01020304050607080102030405060708", "hex");
const CI_PKI_ID1 = Buffer.from(
  "F54172BDF98A95D65CBEB88A38A1C11D800A85C3",
  "hex"
);
const CI_PKI_ID2 = Buffer.from("2122232425262728292A2B2C2D2E2F30313233", "hex");
const CI_PKI_ID3 = Buffer.from("3132333435363738393A3B3C3D3E3F40414243", "hex");
const CI_PKI_ID4 = Buffer.from("4142434445464748494A4B4C4D4E4F50515253", "hex");

const svn = Buffer.from("020100", "hex");

const initiateAuthenticationRequest = Buffer.from(
  "3081910410010203040506070801020304050607080c1d7465737464656661756c74736d6470706c7573312e67736d612e636f6d305e0403020100302b0414f54172bdf98a95d65cbeb88a38a1c11d800a85c304132122232425262728292a2b2c2d2e2f30313233302a04133132333435363738393a3b3c3d3e3f4041424304134142434445464748494a4b4c4d4e4f50515253",
  "hex"
);

const transactionId = crypto.randomBytes(16);

const serverAddress = Buffer.from("testsmdpplus1.gsma.com", "utf-8");

const euiccInfo2 = {
  profileVersion: Buffer.from("020100", "hex"),
  svn,
  euiccFirmwareVer: svn,
  uiccCapability: Buffer.from("10000000000000000000000000", "hex"),
  ts102241Version: Buffer.from("090000", "hex"),
  globalplatformVersion: Buffer.from("020300", "hex"),
  rspCapability: Buffer.from("1000", "hex"),
  euiccCiPKIdListForVerification: [CI_PKI_ID1, CI_PKI_ID2],
  euiccCiPKIdListForSigning: [CI_PKI_ID3, CI_PKI_ID4],
  ppVersion: Buffer.from("010000", "hex"),
  sasAcreditationNumber: Buffer.from("GSMA_SAS_123456789", "utf-8"),
  extCardResource: Buffer.from("81", "hex"),
};

const ctxParams1 = {
  ctxParamsForCommonAuthentication: {
    deviceInfo: {
      tac,
      deviceCapabilities: {
        gsmSupportedRelease: Buffer.from("050000", "hex"),
        utranSupportedRelease: Buffer.from("080000", "hex"),
        cdma2000onexSupportedRelease: Buffer.from("010000", "hex"),
        cdma2000hrpdSupportedRelease: Buffer.from("010000", "hex"),
        cdma2000ehrpdSupportedRelease: Buffer.from("020000", "hex"),
        eutranSupportedRelease: Buffer.from("020000", "hex"),
        contactlessSupportedRelease: Buffer.from("090000", "hex"),
        rspCrlSupportedVersion: Buffer.from("020100", "hex"),
      },
    },
    imei: Buffer.from("0000000011111111", "hex"),
  },
};

xdescribe("asn1 mapper service", () => {
  describe.only("jsToDer", () => {
    test("converts initiateAuthenticationRequest from json to DER format", () => {
      const mapped = {
        euiccChallenge,
        smdpAddress,
        euiccInfo1: {
          svn,
          euiccCiPKIdListForVerification: [CI_PKI_ID1, CI_PKI_ID2],
          euiccCiPKIdListForSigning: [CI_PKI_ID3, CI_PKI_ID4],
        },
      };

      const tree = asn1Mapper.toTree(
        mapped,
        asn_js.InitiateAuthenticationRequest
      );

      const buffer = asn1Tree.encode(tree);

      expect(buffer).toEqual(initiateAuthenticationRequest);
    });

    test("converts authenticateServerResponse from json to DER format", () => {
      const euiccSigned1 = {
        transactionId,
        serverAddress,
        serverChallenge: euiccChallenge,
        euiccInfo2,
        ctxParams1,
      };

      const euiccSigned1Buffer = asn1Services.jsToDer(
        euiccSigned1,
        asn_js.EuiccSigned1
      );

      const euiccSignature1 = signatureServices.signData(
        euiccSigned1Buffer,
        euiccPrivateKey
      );

      const AuthenticateClientRequest = {
        transactionId,
        authenticateServerResponse: {
          authenticateResponseOk: {
            euiccSigned1,
            euiccSignature1,
            euiccCertificate: consts.euicc.cert.der.brp,
            eumCertificate: consts.eum.cert.der.brp,
          },
        },
      };

      const buffer = asn1Services.jsToDer(
        AuthenticateClientRequest,
        asn_js.AuthenticateClientRequest
      );

      expect(buffer).toEqual(AuthenticateClientRequest);
    });

    test("converts AuthenticateClientResponse from json to DER format", () => {
      const profileMetadata = {
        iccid: Buffer.from("989209012143658709F5", "hex"),
        serviceProviderName: Buffer.from("SP Name 1", "utf-8"),
        profileName: Buffer.from("Operational Profile Name 1", "utf-8"),
        profileClass: 1,
      };

      const smdpSigned2 = {
        transactionId,
        ccRequiredFlag: Buffer.from([false]),
        bppEuiccOtpk: eumCert.publicKeyRaw,
      };

      const smdpSigned2Buffer = asn1Services.jsToDer(
        smdpSigned2,
        asn_js.SmdpSigned2
      );

      const smdpSignature2 = signatureServices.signData(
        smdpSigned2Buffer,
        euiccPrivateKey
      );

      const authenticateClientResponseEs9 = {
        authenticateClientOk: {
          transactionId,
          profileMetadata,
          smdpSigned2,
          smdpSignature2,
          smdpCertificate: consts.smdp.pem,
        },
      };

      const buffer = asn1Services.jsToDer(
        authenticateClientResponseEs9,
        asn_js.AuthenticateClientResponseEs9
      );

      expect(buffer).toEqual(authenticateClientResponseEs9);
    });
  });

  describe.only("DerToJs", () => {
    test("converts initiateAuthenticationRequest from DER buffer to json", () => {
      const initiateAuthenticationRequest_json = {
        euiccChallenge,
        smdpAddress,
        euiccInfo1: {
          svn,
          euiccCiPKIdListForVerification: [CI_PKI_ID1, CI_PKI_ID2],
          euiccCiPKIdListForSigning: [CI_PKI_ID3, CI_PKI_ID4],
        },
      };

      const buffer = asn1Tree.decode(initiateAuthenticationRequest);

      const mapped = asn1Mapper.fromTree(
        buffer,
        asn_js.InitiateAuthenticationRequest
      );

      expect(mapped).toEqual(initiateAuthenticationRequest_json);
    });
  });
});
